# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "../tests_helper"
require_relative "../../lib/single_link_node"

class TestSingleLinkNode < Minitest::Test
  prove_it!

  def test_initialize_with_value_only_defaults_next_to_nil
    node = SingleLinkNode.new 4

    assert_equal 4, node.value
    assert_nil node.next
  end

  def test_initialize_with_value_and_next_link
    node = SingleLinkNode.new 1, 2

    assert_equal 1, node.value
    refute_nil node.next
  end

  def test_set_node_value_to_new_value
    node = SingleLinkNode.new 4
    old_value = node.value

    node.value = "hi"

    refute_equal old_value, node.value
  end

  def test_set_node_next_link
    node = SingleLinkNode.new 3

    node.next = SingleLinkNode.new 2

    refute_nil node.next
  end
end
