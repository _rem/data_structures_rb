# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "../tests_helper"
require_relative "../../lib/singly_linked_list"

class TestSinglyLinkedList < Minitest::Test
  prove_it!

  def test_list_can_be_initialized_with_various_type_of_data
    assert SinglyLinkedList.new 1
    assert SinglyLinkedList.new "a"
    assert SinglyLinkedList.new([])
    assert SinglyLinkedList.new({})
  end

  def test_list_has_a_head_element
    list = SinglyLinkedList.new 1
    assert_equal 1, list.head.value
  end

  def test_list_has_a_tail_element
    list = SinglyLinkedList.new "a"
    assert_equal "a", list.tail.value
  end

  def test_appending_data_changes_tail
    list = SinglyLinkedList.new 1
    list.append "a"
    assert_equal "a", list.tail.value

    list.append 2
    assert_equal 2, list.tail.value
  end

  def test_prepending_data_changes_head
    list = SinglyLinkedList.new 1
    list.prepend "a"

    assert_equal "a", list.head.value
    assert_equal 1, list.tail.value
  end

  def test_deleting_first_element_updates_head
    list = SinglyLinkedList.new 1
    list.append "a"

    assert_equal 1, list.delete(1)
    assert_equal "a", list.head.value
  end

  def test_deleting_last_element_updates_tail
    list = SinglyLinkedList.new 1
    list.prepend "a"
    list.prepend 2

    assert_equal 1, list.delete(1)
    assert_equal "a", list.tail.value
  end

  def test_deleting_elements_in_the_middle_of_the_list
    list = SinglyLinkedList.new "a"
    list.append "b"
    list.prepend "c"

    assert_equal "a", list.delete("a")
    assert_equal "c", list.head.value
    assert_equal "b", list.head.next.value
    assert_equal "b", list.tail.value

    list.prepend 1
    list.prepend 2
    list.append 3
    list.append 4

    assert_equal "b", list.delete("b")
    assert_equal 2, list.head.value
    assert_equal 4, list.tail.value
  end

  def test_size_counts_elements_in_list
    list = SinglyLinkedList.new 1
    assert_equal 1, list.size

    list.append "a"
    assert_equal 2, list.size

    list.prepend "b"
    list.prepend 3
    list.append "c"
    list.append 4

    assert_equal 6, list.size
  end
end
