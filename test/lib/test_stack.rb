# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "../tests_helper"
require_relative "../../lib/stack"
require_relative "../../lib/stack_using_array.rb"

class TestStack < Minitest::Test
  prove_it!

  def test_initialized_with_capacity
    stack = Stack.new 1
    assert_equal 1, stack.capacity
  end

  def test_initialized_empty
    stack = Stack.new 4
    assert_equal 0, stack.size
    assert_equal true, stack.empty?
  end

  def test_push_adds_element_to_top
    stack = Stack.new 3
    stack.push 5
    assert_equal 5, stack.top

    stack.push 8
    assert_equal 8, stack.top

    stack.push 3
    assert_equal 3, stack.top
  end

  def test_raises_exception_when_pushing_on_a_full_stack
    stack = Stack.new 1
    stack.push 5
    assert_raises(StackOverflow) { stack.push 8 }
  end

  def test_pop_returns_removed_value
    stack = Stack.new 1
    stack.push 6

    assert_equal 6, stack.pop
  end

  def test_size_returns_total_elements_in_stack
    stack = Stack.new 3
    stack.push 5
    stack.push 8
    assert_equal 2, stack.size

    stack.pop
    assert_equal 1, stack.size

    stack.pop
    assert_equal 0, stack.size
  end

  def test_empty_stack_pops_nil_since_size_is_0
    stack = Stack.new 1
    stack.push 5

    assert_equal 5, stack.pop
    assert_equal 0, stack.size

    assert_nil stack.pop
    assert_equal 0, stack.size
  end
end
