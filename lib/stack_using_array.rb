# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

class StackOverflow < StandardError; end

class Stack
  attr_reader :capacity

  def initialize capacity
    @capacity = capacity
    @ary = []
  end

  def push element
    raise StackOverflow if size >= capacity
    ary.push element
  end

  def top
    ary.last
  end

  def size
    ary.size
  end

  def empty?
    size.zero?
  end

  def pop
    ary.pop
  end

  private

  attr_reader :ary
end
