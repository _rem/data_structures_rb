# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "./singly_linked_list"

class StackOverflow < StandardError; end

class Stack
  attr_reader :capacity

  def initialize capacity
    @capacity = capacity
    @list = new_list nil
  end

  def push element
    raise StackOverflow if size >= capacity
    list.prepend element
  end

  def size
    list.size - 1
  end

  def empty?
    size.zero?
  end

  def top
    list.head.value
  end

  def pop
    unless empty?
      popped = list.head.value
      list.delete popped
    end
    popped
  end

  private

  attr_reader :list

  def new_list value
    SinglyLinkedList.new value
  end
end
