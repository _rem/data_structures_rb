# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

class SingleLinkNode
  attr_accessor :value, :next

  def initialize value, next_element=nil
    self.value = value
    self.next = next_element
  end
end
