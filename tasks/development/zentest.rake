# -*- coding: utf-8 -*-

desc "Analize code"
task :zentest_lib do
  code = FileList["./lib/**/*.rb"]
  tests = FileList["./test/lib/**/test_*.rb"]

  sh "zentest #{code.concat tests}"
end
