# -*- coding: utf-8 -*-

namespace :production do
  desc "Run example code."
  task :run do
    sh "ruby -e \"puts 'Hello World!'\""
  end
end
