# Data Structures Rb

Many work interviews require basic knowledge
on data structures and algorithms. These implementations
are not production ready. I simply put them together
as a way of studying for those interviews.

## Setup Data Structures Rb

In order to get Data Structures Rb up and running for development you need to follow
these steps in your terminal from whichever folder will contain the `data_structures_rb` folder.

```
git clone https://notabug.org/rem/data_structures_rb.git ./data_structures_rb
cd data_structures_rb
cp ./.env.rb.sample ./.env.rb
```

Edit the `.env.rb` file to fit this project.

Install dependencies locally.

```
bundle install --path .bundle/gems
```

If you run into problems chances are you need to sudo-gem-install a few
gems. Check `Gemfile` for details.

## Export terminal session variables

Export your project's environment, as well as the `bin` folder to PATH, whenever
you start a new terminal session. This allows us to run `rake <task>` rather than
`bundle exec rake <task>`.

```
export PROJECT_NAME_ENV="development"
export PATH="$PWD/bin:$PATH"
hash -r 2>/dev/null || true
```

Listing all tasks specially when not all of them are meant for all environments can get
confusing. `rake -T` only lists the tasks available to the specified environment.
If no environment has been explicitly set in the terminal session, rake tasks will
default to development.

You can check what rake tasks are available in a given environment (ie. production).

```
env PROJECT_NAME='production' rake -T
```

Most binstubs execute a command rather than start a session. Hence most are run from
tasks. Those not included in a task are usually run on their own terminal session.

`autotest` Run all tests then only those related to the code you are working on.
`rake <task>` Run task. Binstub prevents prepending `bundle exec`.
`repl` Start a REPL session loading all files in `lib`

If you want to step through some of the code during development use from anywhere:

```
require 'pry-byebug'; binding.pry
```
